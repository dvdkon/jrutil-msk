// This file is part of JrUtil-MSK and is licenced under the GNU GPLv3 or later
// (c) 2019 David Koňařík

open System.IO

open Docopt

open Npgsql

open JrUtil.Utils
open JrUtil.SqlRecordStore
open JrUtil.Gtfs
open JrUtil.GtfsMerge

// TODO: Move functions from here to JrUtil?
open JrUnify.Jdf
open JrUnify.CzPtt
open JrUnify.Utils

let docstring = (fun (s: string) -> s.Trim()) """
jrunify-msk, a tool for processing public transport data

Usage:
    jrunify-msk.exe --connstr=CONNSTR --out=OUT [options]

Options:
    --connstr=CONNSTR              Npgsql connection string
    --out=OUTPATH                  Output directory
    --jdf=PATH                     JDF input directory (extracted)
    --czptt=PATH                   CZPTT SŽDC directory (extracted)
    --czptt-coords=PATH            CSV file with coordinates of train stops
    --cis-coords=PATH              CSV file with coordinates of CIS stops
    --cis-agency-whitelist=PATH    Text file with IDs of CIS agencies (one per line)
    --dpo-gtfs=PATH                DPO GTFS input directory (extracted)
"""

let loadStopCoords (conn: NpgsqlConnection) schema path =
    cleanAndSetSchema conn schema
    executeSql conn """
        CREATE TABLE stop_coords (
            stop_name text PRIMARY KEY,
            lat numeric, lon numeric);

        CREATE UNLOGGED TABLE stop_coords_temp (LIKE stop_coords);
        """ []
    use writer =
        conn.BeginTextImport("COPY stop_coords_temp FROM STDIN (FORMAT CSV)")
    writer.Write(File.ReadAllText(path))
    writer.Close()
    executeSql conn """
        INSERT INTO stop_coords (SELECT * FROM stop_coords_temp)
        ON CONFLICT DO NOTHING;

        DROP TABLE stop_coords_temp;
    """ []

let applyStopCoords (conn: NpgsqlConnection) gtfsSchema stopCoordSchema =
    setSchema conn (gtfsSchema + "," + stopCoordSchema)
    executeSql conn """
        UPDATE stops AS s
        SET lat = c.lat, lon = c.lon
        FROM stop_coords AS c
        WHERE c.stop_name = s.name
        """ []

let loadStopCoordsSr70 (conn: NpgsqlConnection) schema path =
    cleanAndSetSchema conn schema
    executeSql conn """
        CREATE TABLE stop_coords_sr70 (
            sr70 text PRIMARY KEY,
            stop_name text,
            lat numeric, lon numeric);
        """ []
    use writer =
        conn.BeginTextImport("COPY stop_coords_sr70 FROM STDIN (FORMAT CSV)")
    writer.Write(File.ReadAllText(path))

let applyStopCoordsSr70 (conn: NpgsqlConnection) gtfsSchema stopCoordSchema =
    setSchema conn (gtfsSchema + "," + stopCoordSchema)
    executeSql conn """
        UPDATE stops AS s
        SET lat = c.lat, lon = c.lon
        FROM stop_coords_sr70 AS c
        WHERE s.id LIKE CONCAT('-CZPTTS-CZ-', SUBSTRING(c.sr70 FOR 5), '%')
           OR s.id LIKE CONCAT('-CZPTTST-CZ-', SUBSTRING(c.sr70 FOR 5), '%');
        """ []

let processJdf conn whitelistPath path =
    let schemaMerged = "jdf_merged"
    let schemaTemp = "jdf_temp"
    let schemaIntermediate = "jdf_intermediate"

    let whitelist =
        whitelistPath |> Option.map (fun wp -> File.ReadAllLines(wp))
    let whitelistPass (agencyFile: string) =
        match whitelist with
        | Some wl -> wl |> Seq.exists (fun w -> agencyFile.Contains(w))
        | None -> true

    cleanAndSetSchema conn schemaTemp
    cleanAndSetSchema conn schemaMerged
    sqlCreateGtfsTables conn
    let mergedFeed =
        new MergedFeed(conn, schemaMerged, TripMergeStrategy.Never, false)
    cleanAndSetSchema conn schemaIntermediate
    sqlCreateGtfsTables conn

    Directory.EnumerateDirectories(path, "*", SearchOption.AllDirectories)
    |> Seq.iter (fun jdfPath ->
        printfn "Processing JDF: %s" jdfPath
        try
            // This is a crude method of "prefiltering" for performance. As
            // long as it passes all whitelisted files, it can pass some
            // non-whitelisted ones as well
            findPathCaseInsensitive jdfPath "Dopravci.txt"
            |> Option.iter (fun dopravci ->
                let agencyFile = File.ReadAllText(dopravci)
                if whitelistPass agencyFile then
                    setSchema conn schemaIntermediate
                    jdfToGtfsDb conn true jdfPath
                    setSchema conn schemaTemp
                    mergedFeed.InsertFeed schemaIntermediate)
        with
        | :? PostgresException as e ->
            printfn "Error while processing %s:\nSQL error at %s:%s:%d:\n%s"
                    jdfPath e.File e.Line e.Position e.Message
        | e ->
            printfn "Error while processing %s:\n%A" jdfPath e
    )

    applyCisCoords conn schemaMerged

let applyCisAgencyWhitelist (conn: NpgsqlConnection) gtfsSchema whitelistPath =
    setSchema conn gtfsSchema
    executeSql conn """
        CREATE TEMPORARY TABLE cis_agency_whitelist (id text);
    """ []
    use writer =
        conn.BeginTextImport("COPY cis_agency_whitelist FROM STDIN (FORMAT CSV)")
    writer.Write(File.ReadAllText(whitelistPath))
    writer.Close()
    executeSql conn """
        DELETE FROM stoptimes AS st
        USING trips AS t,
              routes AS r
        WHERE t.id = st.tripid
          AND r.id = t.routeid
          AND r.agencyid NOT LIKE ALL (
              SELECT concat('%:JDFA-', w.id, '%')
              FROM cis_agency_whitelist AS w);

        DELETE FROM trips AS t
        USING routes AS r
        WHERE r.id = t.routeid
          AND r.agencyid NOT LIKE ALL (
              SELECT concat('%:JDFA-', w.id, '%')
              FROM cis_agency_whitelist AS w);

        DELETE FROM routes AS r
        WHERE r.agencyid NOT LIKE ALL (
              SELECT concat('%:JDFA-', w.id, '%')
              FROM cis_agency_whitelist AS w);

        DELETE FROM agencies AS a
        WHERE a.id NOT LIKE ALL (
              SELECT concat('%:JDFA-', w.id, '%')
              FROM cis_agency_whitelist AS w);
    """ []

let mergeAll conn =
    cleanAndSetSchema conn "merged"
    sqlCreateGtfsTables conn
    let mergedFeed =
        //new MergedFeed(conn, "merged", TripMergeStrategy.WithRoute, true)
        new MergedFeed(conn, "merged", TripMergeStrategy.Never, true)
    ["jdf_merged"; "czptt_merged"; "dpo_gtfs"]
    |> List.iter (fun schema ->
        try
            cleanAndSetSchema conn "merge_temp"
            mergedFeed.InsertFeed schema
        with
        | :? PostgresException as e ->
            printfn "Error while merging %s:\nSQL error at %s:%s:%d:\n%s\n"
                    schema e.File e.Line e.Position e.Message
        | e ->
            printfn "Error while merging %s:\n%A" schema e
    )

let deleteStopsWithoutCoords conn =
    executeSql conn """
        DELETE FROM stoptimes AS st
        USING stops AS s
        WHERE st.stopid = s.id
          AND (s.lat IS NULL or s.lon IS NULL);

        DELETE FROM stops
        WHERE lat IS NULL or lon IS NULL;

        DELETE FROM stoptimes
        WHERE tripid IN (
            SELECT t.id
            FROM trips AS t
            LEFT JOIN stoptimes AS st ON st.tripid = t.id
            GROUP BY t.id
            HAVING COUNT(st.*) < 2
        );

        DELETE FROM trips
        WHERE id IN (
            SELECT t.id
            FROM trips AS t
            LEFT JOIN stoptimes AS st ON st.tripid = t.id
            WHERE st IS NULL
        );
    """ []

let renameStopIds conn =
    executeSql conn """
        CREATE TEMPORARY TABLE stop_rename_map (
            orig_id text PRIMARY KEY,
            new_id text UNIQUE
        );

        INSERT INTO stop_rename_map
        SELECT id, CONCAT('Z', row_number() OVER (), 'S1')
        FROM stops;

        ALTER TABLE stoptimes DISABLE TRIGGER ALL;
        ALTER TABLE stops DISABLE TRIGGER ALL;

        UPDATE stops
        SET id = new_id
        FROM stop_rename_map
        WHERE orig_id = id;

        UPDATE stops
        SET parentstation = new_id
        FROM stop_rename_map
        WHERE orig_id = parentstation;

        UPDATE stoptimes
        SET stopid = new_id
        FROM stop_rename_map
        WHERE orig_id = stopid;

        ALTER TABLE stops ENABLE TRIGGER ALL;
        ALTER TABLE stoptimes ENABLE TRIGGER ALL;
    """ []

let processDpoGtfs conn path =
    let schema = "dpo_gtfs"

    cleanAndSetSchema conn schema
    sqlCreateGtfsTablesNoConstrs conn
    sqlLoadGtfsFeed conn path

    executeSql conn """
        CREATE FUNCTION new_stop_id(old_id text) RETURNS text LANGUAGE SQL
        AS $$
            SELECT CASE
                WHEN old_id ~ '^Z.*S.*$' THEN
                    regexp_replace(old_id, '^Z(.*)S(.*)$', '-CZPTTS-\1-\2')
                ELSE old_id
            END;
        $$;

        UPDATE stops
        SET id = new_stop_id(id);

        UPDATE stoptimes
        SET stopid = new_stop_id(stopid);

        DROP FUNCTION new_stop_id;
    """ []

    sqlCreateGtfsConstrs conn

let jrunifyMsk connstr out jdf czptt czpttCoords cisCoords cisAgencyWhitelist
               dpoGtfs =
    // Dirty hack to make sure there's no command timeout
    let dbConnStrMod = connstr + ";CommandTimeout=0"
    let newConn () =
        let c = getPostgresqlConnection dbConnStrMod
        c.Notice.Add(fun ev ->
            let n = ev.Notice
            if n.Severity <> "NOTICE" then
                printfn "SQL notice: %s" n.MessageText)
        c.Open()
        c

    [
        async {
            use c = newConn ()
            cisCoords |> Option.iter (fun cc ->
                measureTime "Loading CIS coordinates" (fun () ->
                    loadStopCoords c "cis_coords" cc
            ))
            jdf |> Option.iter (fun jdf ->
                measureTime "Processing JDF" (fun () ->
                    processJdf c cisAgencyWhitelist jdf
            ))
            cisAgencyWhitelist |> Option.iter (fun ciw ->
                measureTime "Processing CIS agency whitelist" (fun () ->
                    applyCisAgencyWhitelist c "jdf_merged" ciw
            ))
        }
        async {
            use c = newConn ()
            czpttCoords |> Option.iter (fun cc ->
                measureTime "Loading CZPTT coordinates" (fun () ->
                    loadStopCoordsSr70 c "czptt_coords" cc
            ))
            czptt |> Option.iter (fun czptt ->
                measureTime "Processing CZPTT" (fun () ->
                    processCzPtt c czptt
                    applyStopCoordsSr70 c "czptt_coords" "czptt_merged"
            ))
        }
        async {
            use c = newConn ()
            dpoGtfs |> Option.iter (fun dpo ->
                measureTime "Loading DPO GTFS" (fun () ->
                    processDpoGtfs c dpo
            ))
        }
    ]
    |> Async.Parallel
    |> Async.RunSynchronously
    |> ignore

    use c = newConn ()
    measureTime "Merging" (fun () ->
        mergeAll c
    )

    measureTime "Deleting stops without coordinates" (fun () ->
        setSchema c "merged"
        deleteStopsWithoutCoords c
    )

    (*measureTime "Renaming stop IDs" (fun () ->
        renameStopIds c
    )*)

    measureTime "Saving" (fun () ->
        saveGtfsSqlSchema c "merged" out
    )

[<EntryPoint>]
let main args =
    withProcessedArgs docstring args (fun args ->
        jrunifyMsk (argValue args "--connstr")
                   (argValue args "--out")
                   (optArgValue args "--jdf")
                   (optArgValue args "--czptt")
                   (optArgValue args "--czptt-coords")
                   (optArgValue args "--cis-coords")
                   (optArgValue args "--cis-agency-whitelist")
                   (optArgValue args "--dpo-gtfs")
        0)
